package generate_random_number;
import org.camunda.bpm.client.ExternalTaskClient;
import org.camunda.bpm.engine.variable.Variables;
import java.util.*;

public class App {

    public static void main(String... args) {
        // bootstrap the client
        ExternalTaskClient client = ExternalTaskClient.create()
                .baseUrl("http://localhost:8080/engine-rest")
                .build();
        // subscribe to the topic
        client.subscribe("generateNumberTopic")
                .lockDuration(1000)
                .handler((externalTask, externalTaskService) -> {
                    // retrieve a variable from the Process Engine
                    int min = externalTask.getVariable("range_min");
                    int max = externalTask.getVariable("range_max");
                    Random random = new Random();
                    int number = random.nextInt((max - min) + 1) + min;
                    // complete the external task
                    externalTaskService.complete(externalTask, Variables.createVariables().putValue("number", number));
                    System.out.println("Random number generated is: " + number);
                    System.out.println("The External Task " + externalTask.getId() + " has been completed!");
                }).open();
    }

}