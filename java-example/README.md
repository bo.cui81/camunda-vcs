# Camunda External Task Worker (Java)

This Java example comes with a simple example process with 2 service tasks.
One generates a random number and the other one prints and posts that number to Slack using the REST API.

After cloning the repo, open the repo in an IDE of your choice. In this demo, we will use IntelliJ.


1. Right-click on `pom.xml` under `java-example`, and choose `Add as Maven Project`
</br>![img_1.png](screen_shot/img_1.png)
2. Double-click on your `pom.xml` file, then on the fight side of your screen, double-click `Lifecycle/install`</br>![img_2.png](screen_shot/img_2.png)
3. In the terminal, you should see a BUILD SUCCESS </br>![img_3.png](screen_shot/img_3.png)
4. Now you can run both `App.java` under `src/main/java/generate_random_number` and `src/main/java/post_number` which are the two external task handlers. 