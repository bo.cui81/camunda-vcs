# -*- coding: utf-8 -*-

import random
import typing

import pycamunda.variable

import worker


if __name__ == '__main__':
    import pycamunda.processdef

    url = 'http://localhost:8080/engine-rest'
    worker_id = '1'

    start_instance = pycamunda.processdef.StartInstance(url=url, key='randomNumberProcess')
    start_instance.add_variable(name='range_min', value=0)
    start_instance.add_variable(name='range_max', value=100)
    start_instance()
