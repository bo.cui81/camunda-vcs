'''Module for library use of HTTP REST requests'''
# pylint: disable=too-many-arguments
from ast import literal_eval
from pathlib import Path
import requests
import yaml

class RESTClient:
    ''' Custom library for high-level REST operations on RestSession'''

    def __init__(self):
        self._session = None

    def new_host(
            self,
            location,
            headers=None,
            cookies=None,
            auth=None,
            timeout=None,
            verify=True
        ):
        ''' Create a new host session for making rest requests
            This sets up the tcp connection with the endpoint you want to make requests to
            and keeps it alive until you are finished

            You can also initialize the session with configuration that needs
            to be applied to all requests made to this host
        '''
        self._session = RestSession(
                headers,
                location,
                cookies,
                auth,
                timeout,
                verify
        )

    def new_host_from_config(self, device_name):
        ''' Imports the given :device_name: settings from devices.yaml in the resources folder
            then establishes a new http session with the host.
        '''
        cfg_path = Path.cwd() / 'resources' / 'devices.yaml'
        with cfg_path.open() as f_ptr:
            try:
                cfg = yaml.safe_load(f_ptr)
            except yaml.YAMLError as exc:
                raise Exception(f'Failed to parse YAML file at {str(cfg_path)}\ndue to error {exc}')

        dev = next((d for d in cfg['devices'] if d['name'] == device_name), None)
        if dev:
            if 'rest' in dev:
                self.new_host_from_dict(dev['rest'], device_name)
            else:
                raise Exception(f'Device {device_name} does not support rest connections')
        else:
            raise Exception(f'Device {device_name} not found in {str(cfg_path)}')

    def new_host_from_dict(self, config):
        ''' Creates a new host connection based on dictionary of params '''
        port = config.get('port')
        location = (
            f'''{config.get('protocol','http')}://'''
            f'''{config['host']}'''
            f'''{':'+str(port) if port else ''}'''
        )
        headers = config.get('headers')
        cookies = config.get('cookies')
        timeout = config.get('timeout')
        verify = config.get('verify', True)
        auth = None
        if 'credentials' in config:
            auth = str((config['credentials']['user'], config['credentials']['password']))

        self.new_rest_host(
            location,
            headers,
            cookies,
            auth,
            timeout,
            verify
        )

    def get_req(self, route, params=None, headers=None):
        ''' Perform a GET request in session '''
        return self._session.request(
            'GET',
            route,
            headers=headers,
            **{'params':params}
        )

    def post_req(self, route, data=None, params=None, data_type='json', headers=None):
        ''' Perform a POST request in session '''
        return self._session.request(
            'POST',
            route,
            **{'params': params},
            headers=headers,
            **{'json' if data_type == 'json' else 'data':data}
        )

    def put_req(self, route, data=None, params=None, data_type='json', headers=None):
        ''' Perform a PUT request in session '''
        return self.request(
            'PUT',
            route,
            **{'params': params},
            headers=headers,
            **{'json' if data_type == 'json' else 'data':data}
        )

    def patch_req(self, route, params=None, data=None, data_type='json', headers=None):
        ''' Perform a PATCH request in session '''
        return self._session.request(
            'PATCH',
            route,
            **{'params': params},
            headers=headers,
            **{'json' if data_type == 'json' else 'data':data}
        )

    def delete_req(self, route, params=None, data=None, data_type='json', headers=None):
        ''' Perform a DELETE request in session '''
        return self._session.request(
            'DELETE',
            route,
            params=params,
            headers=headers,
            **{'json' if data_type == 'json' else 'data':data}
        )

    def get_cookies(self):
        ''' Returns a dictionary object representing all the cookies for a particular session '''
        return self._session.get_cookies_as_dict()

    def add_cookies(self, **kwargs):
        ''' Add a list of key=value pairs to the session's cookies '''
        self._session.add_cookies_to_jar(**kwargs)

    def remove_cookies(self, *args):
        ''' Remove a list of keys from a session's cookies '''
        self._session.remove_cookies_from_jar(*args)

    def get_headers(self):
        ''' Returns a dictionary of all headers provided by session in requests '''
        return self._session.get_headers_dict()

    def add_headers(self, **kwargs):
        ''' Add a list of key val pairs to the session's headers '''
        self._session.add_headers_to_dict(**kwargs)

    def remove_headers(self, *args):
        ''' Removes a list of header names from the session '''
        self._session.remove_headers_from_dict(*args)

    def get_timeout(self):
        ''' Returns the request timeout used by a session '''
        return self._session.timeout

    def set_timeout(self, timeout):
        ''' Sets the request timeout used by a session '''
        self._session.timeout = timeout

class RestSession:
    ''' Helper class for low-level operations with python requests class '''
    def __init__(self, headers, location, cookies, auth, timeout, verify):
        session = requests.Session()
        if headers:
            session.headers.update(headers)
        session.auth = literal_eval(auth) if auth else session.auth
        session.url = location if location[-1] != '/' else location[:-1]
        session.verify = verify
        timeout = literal_eval(timeout) if timeout else None

        self.session = session
        if isinstance(cookies, dict):
            for cookie_name in cookies:
                # use dict syntax to insert values into RequestsCookieJar obj
                self.session.cookies[cookie_name] = cookies[cookie_name]
        self.timeout = timeout

    def _get_url(self, route):
        ''' Formats the host+route into one URL '''
        return f'{self.session.url}{route}' if route.startswith('/') \
            else f'{self.session.url}/{route}'

    def get_cookies_as_dict(self):
        ''' Returns the cookies for this session as a dictionary '''
        return {k:v for k, v in self.session.cookies.get_dict().items() if v is not None}

    def add_cookies_to_jar(self, **kwargs):
        ''' Adds a set of cookies to this session '''
        for cookie_name in kwargs:
            self.session.cookies[cookie_name] = kwargs[cookie_name]

    def remove_cookies_from_jar(self, *args):
        ''' Removes a set of cookies from this session '''
        for name in args:
            self.session.cookies[name] = None

    def get_headers_dict(self):
        ''' Returns the list of headers added to each request by the session '''
        return {k:v for k, v in self.session.headers.items() if v is not None}

    def add_headers_to_dict(self, **kwargs):
        ''' Adds headers to the list provided with each request by the session '''
        self.session.headers.update(kwargs)

    def remove_headers_from_dict(self, *args):
        ''' Removes headers from the list proveded with each request by the session '''
        for header in args:
            self.session.headers[header] = None

    def request(self, method, route, **kwargs):
        ''' Dispatch an HTTP request with the following parameters '''
        return self.session.request(
            method,                                     # HTTP method
            self._get_url(route),                       # Full request URL
            params=kwargs.pop('params', None),           # url query params
            data=kwargs.pop('data', None),               # post www form data (as dict)
            json=kwargs.pop('json', None),               # post json data (as string or dict)
            headers=kwargs.pop('headers', None),         # merged with session headers
            timeout=self.timeout
        )
