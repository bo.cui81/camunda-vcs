# -*- coding: utf-8 -*-

import random
import typing
import os
import pycamunda.variable
from lib import rest

import worker


def generate_random_number(
        range_min: pycamunda.variable.Variable, range_max: pycamunda.variable.Variable
) -> typing.Dict[str, int]:
    try:
        number = random.randrange(range_min.value, range_max.value)
        print('Generated random number:', number)
    except ValueError:
        raise worker.ExternalTaskException(message='invalid input')

    return {'number': number}


def post_number(number: pycamunda.variable.Variable,
                location: pycamunda.variable.Variable,
                route: pycamunda.variable.Variable,
                token: pycamunda.variable.Variable,
                slack_channel: pycamunda.variable.Variable) -> typing.Dict:
    # print number and post via REST
    user_name = os.getlogin()
    stage_output = f'[{user_name}] Workflow received random number - {number.value} - that is large enough'
    print(stage_output)

    client = rest.RESTClient()
    # This is from our Slack client
    header_dict = {'Authorization': f'Bearer {token.value}'}
    data_dict = {'text': stage_output, 'channel': slack_channel.value}
    client.new_host(location.value, headers=header_dict)
    client.post_req(route.value, data=data_dict, data_type='data')

    return {}


if __name__ == '__main__':
    import pycamunda.processdef

    url = 'http://localhost:8080/engine-rest'
    worker_id = '1'

    worker = worker.Worker(url=url, worker_id=worker_id)
    worker.subscribe(
        topic='generateNumberTopic',
        func=generate_random_number,
        variables=['range_min', 'range_max']
    )
    worker.subscribe(
        topic='postNumberTopic',
        func=post_number,
        variables=['number', 'location', 'route', 'token', 'slack_channel']
    )

    worker.run()
