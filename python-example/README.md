# Camunda external task worker

This repository contains a worker class for executing external service tasks. The worker is written in Python and uses the package <a href="https://pypi.org/project/pycamunda/">PyCamunda</a> for communicating with Camunda. It is able to

*  subscribe to multiple external service task topics,
*  request variables from the process instance
*  send variables back to the process instance

This repository comes with a simple example process with 2 service tasks. 
One generates a random number and the other one prints and posts that number to Slack using the REST API. 

After cloning the repo, create a virtual environment with:

- python3 -m venv venv
- source venv/bin/activate
- pip3 install -r requirements.txt   

To run (with python):

- Start the external event listening process by running `external_events.py`
- Start a single run with `run_one.py` or by starting a run from the Camunda `TaskList` UI or from the Modeler 'run' triangle
- Watch the progress of the workflow in the Camunda Cockpit (http://localhost:8080/camunda/app/cockpit/default/#/processes) or in the console