# Camunda external task worker (Node Js)

This repository comes with a simple example process with 2 service tasks.
One generates a random number and the other one prints that number.

Make sure you have Node JS and Npm installed in your local environment.

- `brew install node`

After cloning the repo, install the required node js libraries:

- `cd javascript-example`
- `npm install package.json`

To run (with JavaScript):
- Start generate random number external task handler
  - `node generate_random_number.js`

- Start post number external task handler
  - `node post_number.js`
