const { Client, logger } = require('camunda-external-task-client-js');
const { Variables } = require('camunda-external-task-client-js');
// configuration for the Client:
//  - 'baseUrl': url to the Process Engine
//  - 'logger': utility to automatically log important events
const config = { baseUrl: 'http://localhost:8080/engine-rest', use: logger };
// create a Client instance with custom configuration
const client = new Client(config);
// susbscribe to the topic: 'generateNumberTopic'
client.subscribe('generateNumberTopic', async ({ task, taskService }) => {
  //Retrieve the variables from process
  const min = task.variables.get('range_min');
  const max = task.variables.get('range_max');
  //Do the business logic here. In this task we will generate the random number
  //and set it as an variable to the camunda engine.
  console.log(`Current range is ranging from -> ${min} to ${max}`);
  const randomNumber = Math.floor(Math.random() * (max - min) + min);
  console.log(`Current generated random number is: ${randomNumber}`);

  //Set the variable and attach it to complete method in order for
  //the camunda engine to know.
  const processVariable = new Variables();
  processVariable.set('number', randomNumber);
  await taskService.complete(task, processVariable);
});
