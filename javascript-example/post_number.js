const { Client, logger } = require('camunda-external-task-client-js');
const axios = require('axios');
const { WebClient } = require('@slack/web-api');
const os = require('os');
// configuration for the Client:
//  - 'baseUrl': url to the Process Engine
//  - 'logger': utility to automatically log important events
const config = { baseUrl: 'http://localhost:8080/engine-rest', use: logger };
// create a Client instance with custom configuration
const client = new Client(config);
// susbscribe to the topic: 'postNumberTopic'
client.subscribe('postNumberTopic', async ({ task, taskService }) => {
  // Parameters that will be used:
  // location: url for slack api
  // route: specific endpoint for slack api call
  // token: the token for slack api authentication
  // number: the random number generated from previous task
  // slack_channel: the destination slack channel you want to post to
  const slackClient = new WebClient(task.variables.get('token'));
  const username = process.env.USER
  const message = `[${username}] Workflow received random number - ${task.variables.get('number')} - that is large enough`;
  // Do your business logic here. In this example we will simply log to number, and
  // fetch it to slack using slack api and axois.
  console.log(message);
  const url = `${task.variables.get('location')}/${task.variables.get('route')}`;
  const data = {
    text: message,
    channel: task.variables.get('slack_channel')
  };
  // Do your business logic here. In this example we will post the random number to slack channel.
  // Using axios to do a post request to slack
  await postNumber(url, data, task.variables.get('token'));

  // Using slack web api directly
  // const resp = await slackClient.chat.postMessage(data);
  // try {
  //   if (!resp.ok) {
  //     console.log('Unable to post message to Slack, check your parameters');
  //   } else {
  //     console.log(`Successfully post to Slack channel ${data.channel}`);
  //   }
  // } catch (exception) {
  //   console.log(`Successfully post to Slack channel ${data.channel}`);
  // }

  await taskService.complete(task);
});

async function postNumber(url, data, token) {
  const header = {
    // The token for using Slack API
    Authorization: `Bearer ${token}`
  };
  try {
    const resp = await axios.post(url, data, { headers: header });
    if (resp.status !== 200) {
      console.log('Unable to post message to Slack, check your parameters');
    } else {
      console.log(`Successfully post to Slack channel ${data.channel}`);
    }
  } catch (exception) {
    console.log('Exception Caught->', exception);
  }
}
