# Camunda example project

This repository contains a sample of how to run Camunda with sample code for external task workers in Python, Java, and Javascript.

To get started

*  Clone the repo
*  Download, install, and start the latest version of the Camunda Modeler from https://camunda.com/download/modeler/
*  Make sure Docker is running on your system
*  Start Camunda using the `docker-compose up -d`
*  In the modeler, do `File->Open File...` and browse to select random-number-process.bpmn from the repo
*  Load the random-number-process.bpmn into Camunda using the 'rocket' button in the Modeler
*  See that your workflow is running in the Camunda UI:

    -  Click the link that popped up when you deployed with the 'rocket' button (demo:demo)
	
	OR
	
	-  Log in to http://localhost:8080/camunda/app/cockpit/default/#/login (demo:demo)
	-  Click the 3 in the Process Definitions widget and look for _randomNumberProcess_
	-  Click _randomNumberProcess_ and refresh here when instances of your process are created
	
*  Join Slack channel #ddinc_hackathon to be prepared to see output.   You can ask questions here too.
*  Load and run the sample code according the README in one of the specific language examples
